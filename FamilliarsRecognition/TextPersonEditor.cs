﻿using System;

namespace FamilliarsRecognition
{
    class TextPersonEditor
    {
        Person person;
        Presenter presenter;

        public TextPersonEditor(Person person, Presenter presenter)
        {
            this.person = new Person(person);
            this.presenter = presenter;
        }

        public void EditPerson()
        {
            string input;
            do
            {
                PersonEditMenu();
                input = Console.ReadLine();
                switch (input)
                {
                    case "0":
                        break;
                    case "1":
                        ShowPerson(person);
                        break;
                    case "2":
                        ShowPersonFacts(person);
                        break;
                    case "3":
                        ShowPersonLog(person);
                        break;
                    case "4":
                        AddLog();
                        break;
                    case "5":
                        AddFact();
                        break;
                    case "6":
                        DeleteFact();
                        break;
                    default:
                        Console.WriteLine("Please, enter a number of menu option (example: enter '0' to exit).");
                        break;
                }
            } while (input != "0");
        }
        private void AddLog()
        {
            if (!EnterDateTime(out DateTime dateTime))
                return;
            EnterPersonPhrase(out string personPhrase);
            EnterBotPhrase(out string botPhrase);
            LogLine logLine = new LogLine(person.id, dateTime, personPhrase, botPhrase);
            presenter.AddLog(logLine);
            person.logs.Add(logLine);
        }

        private void AddFact()
        {
            Console.WriteLine("Enter new fact:");
            Fact fact = new Fact(person.id, Console.ReadLine());
            presenter.AddFact(fact);
            person.facts.Add(fact);
        }

        private void DeleteFact()
        {
            string input;
            int index = 0;
            bool wrongInput = true;
            do
            {
                Console.WriteLine("Enter number of a fact to delete:");
                input = Console.ReadLine();
                try
                {
                    index = (int)uint.Parse(input);
                    wrongInput = false;
                }
                catch(FormatException)
                {
                    Console.Write("Wrong input. ");
                    EventLog.Log("User couldn't type a number", EventLog.Type.Exception);
                }
            } while (wrongInput);
            if (person.facts.Count > --index)
            {
                presenter.DeleteFact(person.facts[index]);
                person.facts.RemoveAt(index);
            }
        }

        private void EnterBotPhrase(out string botPhrase)
        {
            Console.WriteLine("Enter person phrase:");
            botPhrase = Console.ReadLine();
        }

        private void EnterPersonPhrase(out string personPhrase)
        {
            Console.WriteLine("Enter person phrase:");
            personPhrase = Console.ReadLine();
        }

        private bool EnterDateTime(out DateTime dateTime)
        {
            bool wrongInput = true;
            do
            {
                dateTime = default;
                Console.WriteLine("Enter log date and time or 0 to cancel:");
                string input = Console.ReadLine();
                if (input == "0")
                    return false;
                try
                {
                    dateTime = DateTime.Parse(input);
                    wrongInput = false;
                }
                catch (FormatException)
                {
                    EventLog.Log("User couldn't enter a date", EventLog.Type.Exception);
                    Console.Write("Wrong input. ");
                }
            } while (wrongInput);
            return true;
        }

        private void PersonEditMenu()
        {
            Console.WriteLine("Enter a number of one of the following options:");
            Console.WriteLine("0. Exit");
            Console.WriteLine("1. Show person");
            Console.WriteLine("2. Show only person facts");
            Console.WriteLine("3. Show only person logs");
            Console.WriteLine("4. Add log");
            Console.WriteLine("5. Add fact");
            Console.WriteLine("6. Delete fact");
        }
        private void ShowPerson(Person person)
        {
            ShowPersonInfo(person);
            ShowPersonFacts(person);
            ShowPersonLog(person);
        }

        private void ShowPersonInfo(Person person)
        {
            Console.WriteLine("Id = " + person.id);
            Console.WriteLine("Name = " + person.name);
            Console.WriteLine("Sex = " + person.sex);
            Console.WriteLine("Birthdate = " + person.birthdate.ToShortDateString());
        }
        private void ShowPersonFacts(Person person)
        {
            Console.WriteLine("\nFacts:");
            int i = 0;
            foreach (Fact fact in person.facts)
                Console.WriteLine(++i + ". " + fact.fact);
        }
        private void ShowPersonLog(Person person)
        {
            int i = 0;
            Console.WriteLine("\nLogs:");
            foreach (LogLine log in person.logs)
                Console.WriteLine(++i + ". " + log.datetime.ToString());
        }
    }
}
