﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FamilliarsRecognition
{
    public partial class FamilliarsRecognitionForm: Form, IInterface
    {
        public Presenter presenter;

        public FamilliarsRecognitionForm()
        {
            EventLog.Log("Application has started");
            InitializeComponent();
            presenter = new Presenter(this);
            entryPath.Text = Config.ImagesPath();
        }

        public void ChangeImage(in Image img)
        {
            cameraDisplay.SetPicture(img);
        }

        public void ClearPeople()
        {
            lbPeople.Items.Clear();
        }

        public void AddPerson(Person person)
        {
            lbPeople.Items.Add(person.ToString());
        }

        public void ChangePerson(int index, Person person)
        {
            lbPeople.Items[index] = person.ToString();
        }

        public void OpenPersonEditor(Person person)
        {
            PersonEditor form = new PersonEditor(person, presenter);
            form.ShowDialog();
        }

        private void btn_next_Click(object sender, EventArgs e) => presenter.NextImage();
        private void btn_prev_Click(object sender, EventArgs e) => presenter.PrevImage();

        private void lbPeople_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = lbPeople.IndexFromPoint(e.Location);
            if (index >= 0)
                presenter.OpenPerson(index);
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            entryPath.BackColor = Color.White;
            if (!presenter.Open(entryPath.Text))
                entryPath.BackColor = Color.Red;
        }
        private void btnAddCommon_Click(object sender, EventArgs e) => presenter.AddCommon();

        private void FormClosingHandler(object sender, FormClosingEventArgs e)
        {
            EventLog.Log("Application has been terminated");
            presenter.Dispose(); // Could be put in Dispose instead
        }

    }
}
