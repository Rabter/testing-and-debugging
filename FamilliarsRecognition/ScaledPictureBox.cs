﻿using System.Drawing;
using System.Windows.Forms;

namespace FamilliarsRecognition
{
    public partial class ScaledPictureBox: PictureBox
    {
        public ScaledPictureBox()
        {
            InitializeComponent();
        }

        public void SetPicture(in Image original)
        {
            Image converted = new Bitmap(original);
            Graphics graphics = Graphics.FromImage(converted);
            graphics.Clear(Color.Black);
            int width, height, offsetX, offsetY;
            if ((double)original.Width / original.Height > (double)this.Width / this.Height)
            {
                width = this.Width;
                height = original.Height * width / original.Width;
                offsetX = 0;
                offsetY = (this.Height - height) / 2;
            }
            else
            {
                height = this.Height;
                width = original.Width * height / original.Height;
                offsetX = (this.Width - width) / 2;
                offsetY = 0;
            }
            graphics.DrawImage(original, new Rectangle(offsetX, offsetY, width, height));
            this.Image = converted;
        }
    }
}
