﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FamilliarsRecognition
{
    public partial class DateTimeEntry: TextBox
    {
        public DateTimeEntry()
        {
            InitializeComponent();
        }
        public bool GetDateTime(out DateTime dateTime)
        {
            try
            {
                dateTime = DateTime.Parse(Text);
                BackColor = Color.White;
                return true;
            }
            catch (FormatException)
            {
                dateTime = default;
                BackColor = Color.Red;
                EventLog.Log("User enetered incorrect datetime", EventLog.Type.Exception);
                return false;
            }
        }
    }
}
