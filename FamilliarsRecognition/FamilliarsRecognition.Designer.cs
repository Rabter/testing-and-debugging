﻿namespace FamilliarsRecognition
{
    partial class FamilliarsRecognitionForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_next = new System.Windows.Forms.Button();
            this.btn_prev = new System.Windows.Forms.Button();
            this.lbPeople = new System.Windows.Forms.ListBox();
            this.cameraDisplay = new FamilliarsRecognition.ScaledPictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.entryPath = new System.Windows.Forms.TextBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnAddCommon = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.cameraDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_next
            // 
            this.btn_next.Location = new System.Drawing.Point(925, 271);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(33, 49);
            this.btn_next.TabIndex = 1;
            this.btn_next.Text = ">";
            this.btn_next.UseVisualStyleBackColor = true;
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // btn_prev
            // 
            this.btn_prev.Location = new System.Drawing.Point(12, 271);
            this.btn_prev.Name = "btn_prev";
            this.btn_prev.Size = new System.Drawing.Size(33, 49);
            this.btn_prev.TabIndex = 2;
            this.btn_prev.Text = "<";
            this.btn_prev.UseVisualStyleBackColor = true;
            this.btn_prev.Click += new System.EventHandler(this.btn_prev_Click);
            // 
            // lbPeople
            // 
            this.lbPeople.FormattingEnabled = true;
            this.lbPeople.Location = new System.Drawing.Point(64, 563);
            this.lbPeople.Name = "lbPeople";
            this.lbPeople.Size = new System.Drawing.Size(841, 95);
            this.lbPeople.TabIndex = 3;
            this.lbPeople.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbPeople_MouseDoubleClick);
            // 
            // cameraDisplay
            // 
            this.cameraDisplay.Location = new System.Drawing.Point(64, 24);
            this.cameraDisplay.Name = "cameraDisplay";
            this.cameraDisplay.Size = new System.Drawing.Size(841, 514);
            this.cameraDisplay.TabIndex = 4;
            this.cameraDisplay.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 670);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Path to images directory: ";
            // 
            // entryPath
            // 
            this.entryPath.Location = new System.Drawing.Point(193, 670);
            this.entryPath.Name = "entryPath";
            this.entryPath.Size = new System.Drawing.Size(296, 20);
            this.entryPath.TabIndex = 6;
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(495, 668);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 23);
            this.btnOpen.TabIndex = 7;
            this.btnOpen.Text = "open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnAddCommon
            // 
            this.btnAddCommon.Location = new System.Drawing.Point(780, 670);
            this.btnAddCommon.Name = "btnAddCommon";
            this.btnAddCommon.Size = new System.Drawing.Size(125, 23);
            this.btnAddCommon.TabIndex = 8;
            this.btnAddCommon.Text = "Add common familliars";
            this.btnAddCommon.UseVisualStyleBackColor = true;
            this.btnAddCommon.Click += new System.EventHandler(this.btnAddCommon_Click);
            // 
            // FamilliarsRecognitionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 703);
            this.Controls.Add(this.btnAddCommon);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.entryPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cameraDisplay);
            this.Controls.Add(this.lbPeople);
            this.Controls.Add(this.btn_prev);
            this.Controls.Add(this.btn_next);
            this.Name = "FamilliarsRecognitionForm";
            this.Text = "Familliars Recognition";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormClosingHandler);
            ((System.ComponentModel.ISupportInitialize)(this.cameraDisplay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.Button btn_prev;
        private System.Windows.Forms.ListBox lbPeople;
        private ScaledPictureBox cameraDisplay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox entryPath;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnAddCommon;
    }
}

