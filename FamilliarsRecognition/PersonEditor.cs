﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace FamilliarsRecognition
{
    public partial class PersonEditor: Form
    {
        Presenter presentor;
        int id;
        public List<LogLine> logs;

        public PersonEditor(in Person person, Presenter presentor)
        {
            InitializeComponent();
            id = person.id;
            foreach (Fact fact in person.facts)
                lb_facts.Items.Add(fact.fact);
            foreach (LogLine log in person.logs)
                lb_logs.Items.Add(log.datetime.ToString());
            pb_profile_photo.SetPicture(person.photo);
            info.Text = "id: " + id + "\nName:\n" + person.name + "\nSex: " + person.sex + "\nBirthdate:\n" + person.birthdate.ToShortDateString();
            logs = person.logs;
            this.presentor = presentor;
        }

        private void btn_add_fact_Click(object sender, EventArgs e)
        {
            string fact = entry_fact.Text;
            presentor.AddFact(new Fact(id, fact));
            lb_facts.Items.Add(fact);
            entry_fact.Clear();
        }
        
        private void btn_delete_fact_Click(object sender, EventArgs e)
        {
            presentor.DeleteFact(new Fact(id, (string)lb_facts.SelectedItem));
            lb_facts.Items.RemoveAt(lb_facts.SelectedIndex);
        }

        private void btn_add_log_Click(object sender, EventArgs e)
        {
            if (entryDateTime.GetDateTime(out DateTime dateTime))
            {
                LogLine log = new LogLine(id, dateTime, entryPersonPhrase.Text, entryBotPhrase.Text);
                presentor.AddLog(log);
                lb_logs.Items.Add(dateTime.ToString());
                logs.Add(log);
                entryPersonPhrase.Clear();
                entryBotPhrase.Clear();
                entryDateTime.Clear();
            }
        }

        private void lb_logs_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = lb_logs.IndexFromPoint(e.Location);
            if (index >= 0)
                showLog(logs[index]);
        }

        private void showLog(LogLine log)
        {
            log_view.Text = log.datetime.ToString() + "\nPerson said:\n" + log.person_phrase + "\n\nBot said:\n" + log.bot_phrase;
        }
    }
}
