﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace FamilliarsRecognition
{
    public class Presenter: IDisposable
    {
        private IInterface ui;
        private InputManager inputManager;
        public FamilliarsManager familliarsManager;
        public List<int> loaded_ids;
        private string currentPath;

        public Presenter(IInterface ui)
        {
            this.ui = ui;
            inputManager = new InputManager();
            familliarsManager = new FamilliarsManager();
            loaded_ids = new List<int>();
        }

        private void ChangeImage(in Image img)
        {
            ui.ChangeImage(img);
            UpdatePeopleList(img);
        }

        private void UpdatePeopleList(in Image img)
        {
            ui.ClearPeople();
            List<int> ids = familliarsManager.HandleNewImage(img);
            foreach (int id in ids)
            {
                familliarsManager.GetPerson(id, out Person person);
                ui.AddPerson(person);
            }
            loaded_ids = ids;
        }

        public bool Open(string path)
        {
            if (inputManager.Open(path))
            {
                inputManager.CurrentImage(out Image img);
                ChangeImage(img);
                currentPath = path;
                return true;
            }
            inputManager.Open(currentPath);
            return false;
        }

        public void NextImage()
        {
            if (inputManager.Ready)
            {
                inputManager.NextImage(out Image img);
                ChangeImage(img);
            }
        }

        public void PrevImage()
        {
            if (inputManager.Ready)
            {
                inputManager.PrevImage(out Image img);
                ChangeImage(img);
            }
        }

        public bool OpenPerson(int index)
        {
            try
            {
                int id = loaded_ids[index];
                familliarsManager.GetPerson(id, out Person person);
                ui.OpenPersonEditor(person);
                familliarsManager.GetPerson(id, out person);
                ui.ChangePerson(index, person);
                return true;
            }
            catch(ArgumentOutOfRangeException)
            {
                EventLog.Log("Attempt to open a person editor to not loaded person", EventLog.Type.Exception);
                return false;
            }
        }

        public void AddLog(in LogLine log) => familliarsManager.AddLog(log);
        public void AddFact(in Fact fact) => familliarsManager.AddFact(fact);
        public void DeleteFact(in Fact fact) => familliarsManager.DeleteFact(fact);
        public void AddCommon()
        {
            if (loaded_ids.Count > 0)
                familliarsManager.AddPairs(loaded_ids);
        }

        public void Dispose() => familliarsManager.Dispose();
    }
}
