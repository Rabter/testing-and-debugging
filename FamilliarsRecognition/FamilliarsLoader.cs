﻿using System.Collections.Generic;
using System.Drawing;

namespace FamilliarsRecognition
{
    class FamilliarsLoader: ILoader
    {
        public List<int> SelectPeople(in Image image, in DBAccess.IStorage manager)
        {
            int id = manager.MinId(), max = manager.MaxId();
            List<int> res = new List<int>(0);
            while (id <= max)
            {
                if (manager.GetImage(id, out Image photo) && Compare(image, photo))
                {
                    res.Add(id);
                }
                id++;
            }
            return res;
        }

        bool Compare(in Image img1, in Image img2)
        {
            img1.Save(comparator_dir + @"\1.jpg"); // ToDo: boost using memory-mapped files
            img2.Save(comparator_dir + @"\2.jpg");
            using (System.Diagnostics.Process process = new System.Diagnostics.Process())
            {
                process.StartInfo.FileName = python_path;
                process.StartInfo.Arguments = comparator_dir + @"\" + comparator_name + " " + comparator_dir + " 1.jpg 2.jpg";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.Start();
                process.WaitForExit();
            }
            return (System.IO.File.ReadAllLines(comparator_dir + @"\result.txt")[0] == "1");
        }

        static string python_path = @"D:\Programs\Python\python.exe";
        static string comparator_dir = @"D:\University\DB_Project\compare";
        static string comparator_name = "compare.py";
    }
}
