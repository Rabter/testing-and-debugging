﻿using System.Drawing;

namespace FamilliarsRecognition
{
    public interface IInterface
    {
        void ChangeImage(in Image img);
        void ClearPeople();
        void AddPerson(Person person);
        void ChangePerson(int index, Person person);
        void OpenPersonEditor(Person person);
    }
}
