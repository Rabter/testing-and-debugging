﻿using System.IO;
using System.Drawing;

namespace FamilliarsRecognition
{
    class InputManager
    {
        public InputManager()
        {
            Ready = false;
        }

        public bool Open(in string imageDirectory)
        {
            try
            {
                images = Directory.GetFiles(imageDirectory);
                current = 0;
                Ready = true;
            }
            catch (System.Exception)
            {
                EventLog.Log("Error while opening images directory", EventLog.Type.Exception);
                Ready = false;
            }
            return Ready;
        }

        public void NextImage(out Image image)
        {
            if (++current >= images.Length)
                current = 0;
            image = Image.FromFile(images[current]);
        }
        public void CurrentImage(out Image image)
        {
            image = Image.FromFile(images[current]);
        }

        public void PrevImage(out Image image)
        {
            if (--current < 0)
                current += images.Length;
            image = Image.FromFile(images[current]);
        }

        string[] images;
        int current;

        public bool Ready { get; private set; }
    }
}
