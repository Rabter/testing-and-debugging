﻿using System.Collections.Generic;
using System.Drawing;

namespace FamilliarsRecognition
{
    public interface ILoader
    {
        List<int> SelectPeople(in Image image, in DBAccess.IStorage manager);
    }
}
