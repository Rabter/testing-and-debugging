﻿namespace FamilliarsRecognition
{
    partial class PersonEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_facts = new System.Windows.Forms.ListBox();
            this.lb_logs = new System.Windows.Forms.ListBox();
            this.log_view = new System.Windows.Forms.RichTextBox();
            this.entryPersonPhrase = new System.Windows.Forms.RichTextBox();
            this.entry_fact = new System.Windows.Forms.TextBox();
            this.btn_add_fact = new System.Windows.Forms.Button();
            this.btn_delete_fact = new System.Windows.Forms.Button();
            this.btn_add_log = new System.Windows.Forms.Button();
            this.entryBotPhrase = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pb_profile_photo = new FamilliarsRecognition.ScaledPictureBox();
            this.info = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.entryDateTime = new FamilliarsRecognition.DateTimeEntry();
            ((System.ComponentModel.ISupportInitialize)(this.pb_profile_photo)).BeginInit();
            this.SuspendLayout();
            // 
            // lb_facts
            // 
            this.lb_facts.FormattingEnabled = true;
            this.lb_facts.Location = new System.Drawing.Point(12, 12);
            this.lb_facts.Name = "lb_facts";
            this.lb_facts.Size = new System.Drawing.Size(120, 134);
            this.lb_facts.TabIndex = 1;
            // 
            // lb_logs
            // 
            this.lb_logs.FormattingEnabled = true;
            this.lb_logs.Location = new System.Drawing.Point(12, 207);
            this.lb_logs.Name = "lb_logs";
            this.lb_logs.Size = new System.Drawing.Size(120, 134);
            this.lb_logs.TabIndex = 3;
            this.lb_logs.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lb_logs_MouseDoubleClick);
            // 
            // log_view
            // 
            this.log_view.Location = new System.Drawing.Point(158, 207);
            this.log_view.Name = "log_view";
            this.log_view.Size = new System.Drawing.Size(170, 134);
            this.log_view.TabIndex = 5;
            this.log_view.Text = "";
            // 
            // entryPersonPhrase
            // 
            this.entryPersonPhrase.Location = new System.Drawing.Point(12, 378);
            this.entryPersonPhrase.Name = "entryPersonPhrase";
            this.entryPersonPhrase.Size = new System.Drawing.Size(149, 96);
            this.entryPersonPhrase.TabIndex = 6;
            this.entryPersonPhrase.Text = "";
            // 
            // entry_fact
            // 
            this.entry_fact.Location = new System.Drawing.Point(12, 152);
            this.entry_fact.Name = "entry_fact";
            this.entry_fact.Size = new System.Drawing.Size(120, 20);
            this.entry_fact.TabIndex = 7;
            // 
            // btn_add_fact
            // 
            this.btn_add_fact.Location = new System.Drawing.Point(158, 152);
            this.btn_add_fact.Name = "btn_add_fact";
            this.btn_add_fact.Size = new System.Drawing.Size(170, 23);
            this.btn_add_fact.TabIndex = 8;
            this.btn_add_fact.Text = "Add fact";
            this.btn_add_fact.UseVisualStyleBackColor = true;
            this.btn_add_fact.Click += new System.EventHandler(this.btn_add_fact_Click);
            // 
            // btn_delete_fact
            // 
            this.btn_delete_fact.Location = new System.Drawing.Point(158, 123);
            this.btn_delete_fact.Name = "btn_delete_fact";
            this.btn_delete_fact.Size = new System.Drawing.Size(170, 23);
            this.btn_delete_fact.TabIndex = 9;
            this.btn_delete_fact.Text = "Delete selected fact";
            this.btn_delete_fact.UseVisualStyleBackColor = true;
            this.btn_delete_fact.Click += new System.EventHandler(this.btn_delete_fact_Click);
            // 
            // btn_add_log
            // 
            this.btn_add_log.Location = new System.Drawing.Point(12, 506);
            this.btn_add_log.Name = "btn_add_log";
            this.btn_add_log.Size = new System.Drawing.Size(316, 23);
            this.btn_add_log.TabIndex = 10;
            this.btn_add_log.Text = "Add log line";
            this.btn_add_log.UseVisualStyleBackColor = true;
            this.btn_add_log.Click += new System.EventHandler(this.btn_add_log_Click);
            // 
            // entryBotPhrase
            // 
            this.entryBotPhrase.Location = new System.Drawing.Point(167, 378);
            this.entryBotPhrase.Name = "entryBotPhrase";
            this.entryBotPhrase.Size = new System.Drawing.Size(161, 96);
            this.entryBotPhrase.TabIndex = 13;
            this.entryBotPhrase.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 359);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Person:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(174, 359);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Bot:";
            // 
            // pb_profile_photo
            // 
            this.pb_profile_photo.Location = new System.Drawing.Point(158, 12);
            this.pb_profile_photo.Name = "pb_profile_photo";
            this.pb_profile_photo.Size = new System.Drawing.Size(77, 97);
            this.pb_profile_photo.TabIndex = 16;
            this.pb_profile_photo.TabStop = false;
            // 
            // info
            // 
            this.info.AutoSize = true;
            this.info.Location = new System.Drawing.Point(241, 12);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(0, 13);
            this.info.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(85, 483);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Date and time:";
            // 
            // entryDateTime
            // 
            this.entryDateTime.Location = new System.Drawing.Point(168, 480);
            this.entryDateTime.Name = "entryDateTime";
            this.entryDateTime.Size = new System.Drawing.Size(100, 20);
            this.entryDateTime.TabIndex = 20;
            // 
            // PersonEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 541);
            this.Controls.Add(this.entryDateTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.info);
            this.Controls.Add(this.pb_profile_photo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.entryBotPhrase);
            this.Controls.Add(this.btn_add_log);
            this.Controls.Add(this.btn_delete_fact);
            this.Controls.Add(this.btn_add_fact);
            this.Controls.Add(this.entry_fact);
            this.Controls.Add(this.entryPersonPhrase);
            this.Controls.Add(this.log_view);
            this.Controls.Add(this.lb_logs);
            this.Controls.Add(this.lb_facts);
            this.Name = "PersonEditor";
            this.Text = "Edit person info";
            ((System.ComponentModel.ISupportInitialize)(this.pb_profile_photo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lb_facts;
        private System.Windows.Forms.ListBox lb_logs;
        private System.Windows.Forms.RichTextBox log_view;
        private System.Windows.Forms.RichTextBox entryPersonPhrase;
        private System.Windows.Forms.TextBox entry_fact;
        private System.Windows.Forms.Button btn_add_fact;
        private System.Windows.Forms.Button btn_delete_fact;
        private System.Windows.Forms.Button btn_add_log;
        private System.Windows.Forms.RichTextBox entryBotPhrase;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private ScaledPictureBox pb_profile_photo;
        private System.Windows.Forms.Label info;
        private System.Windows.Forms.Label label3;
        private DateTimeEntry entryDateTime;
    }
}