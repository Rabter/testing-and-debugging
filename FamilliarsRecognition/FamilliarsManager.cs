﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace FamilliarsRecognition
{
    public class FamilliarsManager: IDisposable
    {
        public FamilliarsManager()
        {
            db_manager = new DBAccess.PeopleStorage();
            loader = new FamilliarsLoader();
        }

        public void Dispose() => db_manager.Dispose();

        public List<int> HandleNewImage(in Image image)
        {
            List<int> people = loader.SelectPeople(image, db_manager);
            db_manager.LoadPeople(people);
            return people;
        }

        public bool GetPerson(int id, out Person person)
        {
            DBAccess.Person factless_person;
            if (!db_manager.GetPerson(id, out factless_person))
            {
                person = default;
                return false;
            }
            person = new Person(factless_person);
            person.facts = new List<Fact>();
            foreach (DBAccess.Fact fact in db_manager.GetFacts(id))
                person.facts.Add(new Fact(fact));
            person.logs = new List<LogLine>();
            foreach (DBAccess.LogLine log in db_manager.GetLogs(id))
                person.logs.Add(new LogLine(log));
            db_manager.GetImage(id, out person.photo);
            return true;
        }

        public void AddPairs(List<int> ids)
        {
            int sizedec = ids.Count - 1;
            for (int i = 0; i < sizedec; ++i)
                for (int j = i + 1; j < ids.Count; ++j)
                    db_manager.AddPair(ids[i], ids[j]);
        }

        public void AddFact(in Fact fact)
        {
            db_manager.AddFact(fact);
        }

        public void DeleteFact(in Fact fact)
        {
            db_manager.DeleteFact(fact);
        }

        public void AddLog(in LogLine log)
        {
            db_manager.AddLog(log);
        }

        public DBAccess.IStorage db_manager;
        public ILoader loader;
    }

    public class Person: DBAccess.Person
    {
        public Person(DBAccess.Person parent)
        {
            id = parent.id;
            name = parent.name;
            sex = parent.sex;
            birthdate = parent.birthdate;
            facts = null;
            logs = default;
            photo = default;
        }

        ~Person()
        {
            if (photo != null)
                photo.Dispose();
        }

        public override string ToString()
        {
            string split = "; ";
            string res = id.ToString() + split + name + split + sex + split + birthdate.ToShortDateString();
            foreach (Fact fact in facts)
                res += split + fact.fact;
            return res;
        }

        public override bool Equals(Object obj)
        {
            if (obj is Person)
            {
                var that = obj as Person;
                return id == that.id && name == that.name && sex == that.sex && birthdate == that.birthdate
                    && new HashSet<Fact>(facts).SequenceEqual(that.facts) && new HashSet<LogLine>(logs).SequenceEqual(that.logs);
            }

            return false;
        }
        public override int GetHashCode()
        {
            return id.GetHashCode() ^ name.GetHashCode() ^ sex.GetHashCode() ^ birthdate.GetHashCode() ^ facts.GetHashCode() ^ logs.GetHashCode();
        }

        public List<Fact> facts;
        public List<LogLine> logs;
        public Image photo;
    }

    public class LogLine: DBAccess.LogLine
    {
        public LogLine(in DBAccess.LogLine log): base(log) {}
        public LogLine(int person_id, in DateTime datetime, in string person_phrase, in string bot_phrase):
            base(person_id, datetime, person_phrase, bot_phrase) {}

        public override bool Equals(Object obj)
        {
            if (obj is LogLine)
            {
                var that = obj as LogLine;
                return person_id == that.person_id && datetime == that.datetime && person_phrase == that.person_phrase && bot_phrase == that.bot_phrase;
            }

            return false;
        }
        public override int GetHashCode()
        {
            return person_id.GetHashCode() ^ datetime.GetHashCode() ^ person_phrase.GetHashCode() ^ bot_phrase.GetHashCode();
        }
    }
    public class Fact: DBAccess.Fact
    {
        public Fact(in DBAccess.Fact fact): base(fact) {}
        public Fact(int person_id, in string fact): base(person_id, fact) {}

        public override bool Equals(Object obj)
        {
            if (obj is Fact)
            {
                var that = obj as Fact;
                return person_id == that.person_id && fact == that.fact;
            }

            return false;
        }
        public override int GetHashCode()
        {
            return person_id.GetHashCode() ^ fact.GetHashCode();
        }
    }

}
