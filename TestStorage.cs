﻿using DBAccess;
using System;
using System.Collections.Generic;
using System.Drawing;


public class TestStorage : IStorage
{
    public bool returnPerson;
    public StorageMockResult mock;
    public PersonInfo info;
    public PersonBuilder person;
    public int minId, maxId;

    public TestStorage()
    {
        returnPerson = false;
        info = default;
        minId = maxId = 0;
    }

    public void AddFact(Fact fact)
    {
        mock.fact = fact;
    }

    public void AddLog(LogLine log)
    {
        mock.log = log;
    }

    public bool AddPair(int id1, int id2)
    {
        mock.pairs.Add(new Tuple<int, int>(id1, id2));
        return true;
    }

    public void DeleteFact(Fact fact)
    {
        mock.deleted = fact;
    }

    public void Dispose()
    {
        return;
    }

    public List<Fact> GetFacts(int id)
    {
        if (returnPerson)
            return info.facts;
        else
            return default;
    }

    public bool GetImage(int id, out Image image)
    {
        if (returnPerson)
        {
            image = info.photo;
            return true;
        }
        else
        {
            image = default;
            return false;
        }
    }

    public List<LogLine> GetLogs(int id)
    {
        if (returnPerson)
            return info.logs;
        else
            return default;
    }

    public bool GetPerson(int id, out Person person)
    {
        if (returnPerson)
        {
            person = this.person.WithId(id).Build();
            return true;
        }
        else
        {
            person = default;
            return false;
        }
    }

    public void LoadPeople(List<int> ids)
    {
        mock.peopleLoaded = ids;
    }

    public int MaxId()
    {
        return maxId;
    }

    public int MinId()
    {
        return minId;
    }
}

public class StorageMockResult
{
    public List<int> peopleLoaded;
    public Fact fact, deleted;
    public LogLine log;
    public List<Tuple<int, int>> pairs;
}

