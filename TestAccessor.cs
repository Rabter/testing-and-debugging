﻿using System;
using System.Collections.Generic;
using System.Drawing;


public class TestAccessor : DBAccess.IAccessor
{
    public bool returnPerson;
    public int minId, maxId;
    public PersonInfo info;
    public PersonBuilder person;
    public AccessorMockResult mock;


    public TestAccessor()
    {
        person = default;
        info = default;
        returnPerson = false;
        minId = maxId = 0;
    }

    public bool GetPerson(ref DBAccess.Person person)
    {
        if (returnPerson)
        {
            person = this.person.WithId(person.id).Build();
            return true;
        }
        else
            return false;
    }

    public bool GetName(int id, out string name)
    {
        if (returnPerson)
        {
            name = person.Build().name;
            return true;
        }
        else
        {
            name = "";
            return false;
        }
    }

    public bool GetImage(int id, out Image image)
    {
        if (returnPerson)
        {
            image = info.photo;
            return true;
        }
        else
        {
            image = default;
            return false;
        }
    }

    public bool GetSex(int id, out string sex)
    {
        if (returnPerson)
        {
            sex = person.Build().sex;
            return true;
        }
        else
        {
            sex = "";
            return false;
        }
    }

    public bool GetBirthdate(int id, out DateTime birthdate)
    {
        if (returnPerson)
        {
            birthdate = person.Build().birthdate;
            return true;
        }
        else
        {
            birthdate = default;
            return false;
        }
    }

    public List<DBAccess.Fact> GetFacts(int id)
    {
        if (returnPerson)
            return info.facts;
        else
            return default;
    }

    public List<DBAccess.LogLine> GetLogs(int person_id)
    {
        if (returnPerson)
            return info.logs;
        else
            return default;
    }

    public bool AddPair(int id1, int id2)
    {
        mock.pair = new Tuple<int, int>(id1, id2);
        return true;
    }

    public bool AddFact(DBAccess.Fact fact)
    {
        mock.fact = fact;
        return true;
    }

    public bool AddLog(DBAccess.LogLine log)
    {
        mock.log = log;
        return true;
    }

    public void DeleteFact(DBAccess.Fact fact)
    {
        mock.deleted = fact;
        return;
    }

    public int MinId()
    {
        return minId;
    }

    public int MaxId()
    {
        return maxId;
    }
}

public class AccessorMockResult
{
    public Tuple<int, int> pair;
    public DBAccess.Fact fact, deleted;
    public DBAccess.LogLine log;
}