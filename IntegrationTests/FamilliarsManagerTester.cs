﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IntegrationTests
{
    [TestClass]
    public class FamilliarsManagerTester
    {
        FamilliarsRecognition.FamilliarsManager manager;
        TestAccessor accessor;

        PersonInfo info;

        List<FamilliarsRecognition.Fact> facts;
        List<FamilliarsRecognition.LogLine> logs;

        FactBuilder factBuilder;
        LogBuilder logBuilder;
        FullPersonBuilder personBuilder;

        public FamilliarsManagerTester()
        {
        }

        [TestInitialize]
        public void Init()
        {
            manager = new FamilliarsRecognition.FamilliarsManager();
            personBuilder = new FullPersonBuilder();
            accessor = new TestAccessor();

            List<DBAccess.Fact> infoFacts;
            List<DBAccess.LogLine> infoLogs;
            facts = new List<FamilliarsRecognition.Fact>();
            logs = new List<FamilliarsRecognition.LogLine>();
            infoFacts = new List<DBAccess.Fact>();
            infoLogs = new List<DBAccess.LogLine>();

            factBuilder = new FactBuilder();
            logBuilder = new LogBuilder();

            infoFacts.Add(factBuilder.WithId(1).WithFact("test fact1").Build());
            facts.Add(new FamilliarsRecognition.Fact(factBuilder.Build()));
            infoFacts.Add(factBuilder.WithFact("test fact2").Build());
            facts.Add(new FamilliarsRecognition.Fact(factBuilder.Build()));

            infoLogs.Add(logBuilder.WithId(1).WithDateTime(new DateTime(2000, 1, 1)).WithPersonPhrase("person test log1").WithBotPhrase("bot test log1").Build());
            logs.Add(new FamilliarsRecognition.LogLine(logBuilder.Build()));
            infoLogs.Add(logBuilder.WithDateTime(new DateTime(2000, 1, 2)).WithPersonPhrase("person test log2").WithBotPhrase("bot test log2").Build());
            logs.Add(new FamilliarsRecognition.LogLine(logBuilder.Build()));

            personBuilder.facts = facts;
            personBuilder.logs = logs;

            info = new PersonInfo();
            info.facts = infoFacts;
            info.logs = infoLogs;
            info.photo = default;

            accessor.info = info;

            DBAccess.PeopleStorage storage = new DBAccess.PeopleStorage();
            storage.accessor = accessor;
            manager.db_manager = storage;
        }

        [TestMethod]
        public void Test1()
        {
            accessor.returnPerson = true;
            personBuilder.WithFacts(facts).WithLogs(logs)
                .WithId(1)
                .WithBirthdate(new DateTime(1111, 11, 11))
                .WithName("Test Person")
                .WithSex("m");
            accessor.person = personBuilder.ToPersonBuilder();
            FamilliarsRecognition.Person result;

            manager.GetPerson(1, out result);

            Assert.AreEqual(result, personBuilder.Build());
        }

        [TestMethod]
        public void Test2()
        {
            accessor.returnPerson = false;
            FamilliarsRecognition.Person result;

            manager.GetPerson(1, out result);

            Assert.AreEqual(result, default);
        }

        [TestMethod]
        public void Test3()
        {
            TestLoader loader = new TestLoader();
            List<int> expected = new List<int>() { 1, 2, 3, 4 };
            loader.result = expected;
            manager.loader = loader;

            List<int> result = manager.HandleNewImage(default);

            Assert.IsTrue(new HashSet<int>(result).SetEquals(expected));
        }

        [TestMethod]
        public void Test4()
        {
            manager.db_manager.LoadPeople(new List<int>(0));
            TestLoader loader = new TestLoader();
            TestAccessor accessor = new TestAccessor();
            accessor.person = personBuilder.ToPersonBuilder();
            accessor.info = info;
            FamilliarsRecognition.Person person;
            accessor.returnPerson = true;
            List<int> expected = new List<int>() { 1, 2, 3, 4 };
            loader.result = expected;
            manager.loader = loader;
            DBAccess.PeopleStorage storage = new DBAccess.PeopleStorage();
            storage.accessor = accessor;
            manager.db_manager = storage;

            List<int> result = manager.HandleNewImage(default);

            Assert.IsTrue(new HashSet<int>(result).SetEquals(expected));
            foreach (int i in expected)
            {
                manager.GetPerson(i, out person);
                Assert.AreEqual(person, personBuilder.WithId(i).Build());
            }
            accessor.returnPerson = false;
            manager.GetPerson(5, out person);
            Assert.AreEqual(person, default);
        }

        public void TestAll()
        {
            Console.WriteLine("Running integration tests...");

            Test1();
            Test2();
            Test3();
            Test4();

            Console.WriteLine("");
        }
    }
}
