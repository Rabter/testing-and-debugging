﻿using System;
using System.Collections.Generic;
using System.Drawing;

public class FactBuilder
{
    int id;
    string fact;

    public FactBuilder()
    {
        id = 0;
        fact = "";
    }

    public FactBuilder WithId(int id)
    {
        this.id = id;
        return this;
    }

    public FactBuilder WithFact(string fact)
    {
        this.fact = fact;
        return this;
    }

    public DBAccess.Fact Build()
    {
        return new DBAccess.Fact(id, fact);
    }

}

public class LogBuilder
{
    int id;
    DateTime datetime;
    string bot, person;

    public LogBuilder()
    {
        id = 0;
        datetime = default;
        bot = "";
        person = "";
    }

    public LogBuilder WithId(int id)
    {
        this.id = id;
        return this;
    }

    public LogBuilder WithDateTime(DateTime datetime)
    {
        this.datetime = datetime;
        return this;
    }

    public LogBuilder WithPersonPhrase(string phrase)
    {
        person = phrase;
        return this;
    }

    public LogBuilder WithBotPhrase(string phrase)
    {
        bot = phrase;
        return this;
    }

    public DBAccess.LogLine Build()
    {
        return new DBAccess.LogLine(id, datetime, person, bot);
    }
}

public class PersonBuilder
{
    int id;
    string name, sex;
    DateTime birthdate;

    public PersonBuilder()
    {
        id = 0;
        name = "";
        sex = "";
        birthdate = default;
    }

    public PersonBuilder WithId(int id)
    {
        this.id = id;
        return this;
    }

    public PersonBuilder WithName(string name)
    {
        this.name = name;
        return this;
    }

    public PersonBuilder WithSex(string sex)
    {
        this.sex = sex;
        return this;
    }
    public PersonBuilder WithBirthdate(DateTime birthdate)
    {
        this.birthdate = birthdate;
        return this;
    }

    public DBAccess.Person Build()
    {
        return new DBAccess.Person()
        {
            id = id,
            name = name,
            sex = sex,
            birthdate = birthdate
        };
    }
}

public class PersonInfo
{
    public List<DBAccess.Fact> facts;
    public List<DBAccess.LogLine> logs;
    public Image photo;
}

public class FullPersonBuilder
{
    int id;
    string name, sex;
    DateTime birthdate;
    public List<FamilliarsRecognition.Fact> facts;
    public List<FamilliarsRecognition.LogLine> logs;
    public Image photo;

    public FullPersonBuilder()
    {
        id = 0;
        name = "";
        sex = "";
        birthdate = default;
        facts = new List<FamilliarsRecognition.Fact>();
        logs = new List<FamilliarsRecognition.LogLine>();
        photo = default;
    }

    public FullPersonBuilder WithId(int id)
    {
        this.id = id;
        return this;
    }

    public FullPersonBuilder WithName(string name)
    {
        this.name = name;
        return this;
    }

    public FullPersonBuilder WithSex(string sex)
    {
        this.sex = sex;
        return this;
    }
    public FullPersonBuilder WithBirthdate(DateTime birthdate)
    {
        this.birthdate = birthdate;
        return this;
    }

    public FullPersonBuilder WithFacts(List<FamilliarsRecognition.Fact> facts)
    {
        this.facts = new List<FamilliarsRecognition.Fact>(facts);
        return this;
    }

    public FullPersonBuilder WithLogs(List<FamilliarsRecognition.LogLine> logs)
    {
        this.logs = new List<FamilliarsRecognition.LogLine>(logs);
        return this;
    }

    public FullPersonBuilder WithImage(Image img)
    {
        photo = img;
        return this;
    }

    public FamilliarsRecognition.Person Build()
    {
        FamilliarsRecognition.Person person = new FamilliarsRecognition.Person(
            new DBAccess.Person()
            {
                id = id,
                name = name,
                sex = sex,
                birthdate = birthdate
            });
        person.facts = facts;
        person.logs = logs;
        person.photo = photo;
        return person;
    }

    public PersonBuilder ToPersonBuilder()
    {
        return new PersonBuilder().WithId(id).WithName(name).WithSex(sex).WithBirthdate(birthdate);
    }
}