﻿if db_id('Familliars') is null
    create database Familliars;
go

use Familliars;
go

drop table Facts;
drop table Logs;
drop table CommonFamilliars;
drop table People;
go

set dateformat dmy;
go

create table People(
    id int not NULL primary key,
	name varchar(20),
	sex char(1),
	birthdate date,
	picture image
	)
go

create table Facts(
	person_id int not NULL,
	fact varchar(40),
	foreign key (person_id) references People(id)
	)
go

create table Logs(
	person_id int not NULL,
	log_time datetime,
	person_phrase text default '',
	bot_phrase text default '',
	foreign key (person_id) references People(id)
	)
go

create table CommonFamilliars(
    id1 int not null,
	id2 int not null
	)
go

bulk insert People from 'D:\University\DB_Project\Database\People.dat'
with (datafiletype = 'char', fieldterminator = ';', keepnulls);
go

bulk insert Facts from 'D:\University\DB_Project\Database\Facts.dat'
with (datafiletype = 'char', fieldterminator = ';', keepnulls);
go

bulk insert Logs from 'D:\University\DB_Project\Database\Logs.dat'
with (datafiletype = 'char', fieldterminator = ';');
go

 update People set [picture] = (select
 MyImage.* from openrowset(bulk
 'D:\University\DB_Project\Database\Sheldon.jpg', single_Blob) MyImage)
 where id = 2;
 go

 update People set [picture] = (select
 MyImage.* from openrowset(bulk
 'D:\University\DB_Project\Database\Leonard.jpg', single_Blob) MyImage)
 where id = 4;
 go
 
 update People set [picture] = (select
 MyImage.* from openrowset(bulk
 'D:\University\DB_Project\Database\Sarah.jpg', single_Blob) MyImage)
 where id = 3;
 go

 update People set [picture] = (select
 MyImage.* from openrowset(bulk
 'D:\University\DB_Project\Database\Bernadette.jpg', single_Blob) MyImage)
 where id = 5;
 go
 
 update People set [picture] = (select
 MyImage.* from openrowset(bulk
 'D:\University\DB_Project\Database\Howard.jpg', single_Blob) MyImage)
 where id = 6;
 go
 
 update People set [picture] = (select
 MyImage.* from openrowset(bulk
 'D:\University\DB_Project\Database\Amy.jpg', single_Blob) MyImage)
 where id = 7;
 go
 
 update People set [picture] = (select
 MyImage.* from openrowset(bulk
 'D:\University\DB_Project\Database\Raj.png', single_Blob) MyImage)
 where id = 8;
 go
 
 update People set [picture] = (select
 MyImage.* from openrowset(bulk
 'D:\University\DB_Project\Database\Penny.jpg', single_Blob) MyImage)
 where id = 9;
 go

select * from People;
select * from Facts;
select * from Logs;
select * from CommonFamilliars;