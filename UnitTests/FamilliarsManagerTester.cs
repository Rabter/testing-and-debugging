﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class FamilliarsManagerTester
    {
        FamilliarsRecognition.FamilliarsManager manager;
        TestStorage storage;
        TestLoader loader;

        FactBuilder factBuilder;
        LogBuilder logBuilder;
        FullPersonBuilder personBuilder;

        PersonInfo info;
        List<DBAccess.Fact> dbFacts;
        List<DBAccess.LogLine> dbLogs;
        List<FamilliarsRecognition.Fact> facts;
        List<FamilliarsRecognition.LogLine> logs;

        StorageMockResult mock;

        public FamilliarsManagerTester()
        {
        }

        [TestInitialize]
        public void Init()
        {
            storage = new TestStorage();

            mock = new StorageMockResult();
            storage.mock = mock;

            personBuilder = new FullPersonBuilder();
            personBuilder.WithId(1)
                .WithBirthdate(new DateTime(1111, 11, 11))
                .WithName("Test Person")
                .WithSex("f");
            storage.person = personBuilder.ToPersonBuilder();

            factBuilder = new FactBuilder();
            logBuilder = new LogBuilder();

            facts = new List<FamilliarsRecognition.Fact>();
            dbFacts = new List<DBAccess.Fact>();
            dbFacts.Add(factBuilder.WithId(1).WithFact("test fact1").Build());
            facts.Add(new FamilliarsRecognition.Fact(factBuilder.Build()));
            dbFacts.Add(factBuilder.WithFact("test fact2").Build());
            facts.Add(new FamilliarsRecognition.Fact(factBuilder.Build()));

            logs = new List<FamilliarsRecognition.LogLine>();
            dbLogs = new List<DBAccess.LogLine>();
            dbLogs.Add(logBuilder.WithId(1).WithDateTime(new DateTime(2000, 1, 1)).WithPersonPhrase("person test log1").WithBotPhrase("bot test log1").Build());
            logs.Add(new FamilliarsRecognition.LogLine(logBuilder.Build()));
            dbLogs.Add(logBuilder.WithDateTime(new DateTime(2000, 1, 2)).WithPersonPhrase("person test log2").WithBotPhrase("bot test log2").Build());
            logs.Add(new FamilliarsRecognition.LogLine(logBuilder.Build()));

            personBuilder.WithFacts(facts);
            personBuilder.WithLogs(logs);

            info = new PersonInfo();
            info.facts = dbFacts;
            info.logs = dbLogs;
            info.photo = default;

            storage.info = info;
            loader = new TestLoader();

            manager = new FamilliarsRecognition.FamilliarsManager();
            manager.db_manager = storage;
            manager.loader = loader;
        }

        [TestMethod]
        public void Test1()
        {
            List<int> expected = new List<int>() { 1, 2, 3, 5 };
            loader.result = expected;

            List<int> res = manager.HandleNewImage(default);

            Assert.AreEqual(res, expected);
            Assert.AreEqual(storage.mock.peopleLoaded, expected);
        }

        [TestMethod]
        public void Test2()
        {
            List<int> expected = new List<int>() { 1, 3, 5 };

            loader.result = new List<int>() { 1, 2, 3, 5 };
            manager.HandleNewImage(default);
            loader.result = expected;
            List<int> res = manager.HandleNewImage(default);

            Assert.AreEqual(res, expected);
            Assert.AreEqual(storage.mock.peopleLoaded, expected);
        }

        [TestMethod]
        public void Test3()
        {
            storage.returnPerson = true;
            FamilliarsRecognition.Person result;
            personBuilder.WithId(2);

            manager.GetPerson(2, out result);

            Assert.AreEqual(result, personBuilder.Build());
        }

        [TestMethod]
        public void Test4()
        {
            storage.returnPerson = false;
            FamilliarsRecognition.Person result;
            personBuilder.WithId(2);

            manager.GetPerson(2, out result);

            Assert.AreEqual(result, default);
        }

        [TestMethod]
        public void Test5()
        {
            mock.pairs = new List<Tuple<int, int>>();
            List<int> ids = new List<int>() { 1, 3, 4, 6 };
            List<Tuple<int, int>> expected = new List<Tuple<int, int>>() {
                new Tuple<int, int>(1, 3), new Tuple<int, int>(1, 4), new Tuple<int, int>(1, 6),
                new Tuple<int, int>(3, 4), new Tuple<int, int>(3, 6),
                new Tuple<int, int>(4, 6)
            };

            manager.AddPairs(ids);

            Assert.IsTrue(new HashSet<Tuple<int, int>>(expected).SetEquals(mock.pairs));
        }

        [TestMethod]
        public void Test6()
        {
            mock.pairs = new List<Tuple<int, int>>();
            List<int> ids = new List<int>(0);
            List<Tuple<int, int>> expected = new List<Tuple<int, int>>(0);

            manager.AddPairs(ids);

            Assert.IsTrue(new HashSet<Tuple<int, int>>(expected).SetEquals(mock.pairs));
        }

        [TestMethod]
        public void Test7()
        {
            mock.pairs = new List<Tuple<int, int>>();
            List<int> ids = new List<int>() { 1 };
            List<Tuple<int, int>> expected = new List<Tuple<int, int>>(0);

            manager.AddPairs(ids);

            Assert.IsTrue(new HashSet<Tuple<int, int>>(expected).SetEquals(mock.pairs));
        }

        [TestMethod]
        public void Test8()
        {
            mock.log = default;
            FamilliarsRecognition.LogLine expected = new FamilliarsRecognition.LogLine(logBuilder
                .WithId(2)
                .WithDateTime(new DateTime(1010, 10, 10))
                .WithBotPhrase("bot test")
                .WithPersonPhrase("person test").Build());

            manager.AddLog(expected);

            Assert.AreEqual(mock.log, expected);
        }

        [TestMethod]
        public void Test9()
        {
            mock.fact = default;
            FamilliarsRecognition.Fact expected = new FamilliarsRecognition.Fact(factBuilder.WithId(1).WithFact("test").Build());

            manager.AddFact(expected);

            Assert.AreEqual(mock.fact, expected);
        }

        [TestMethod]
        public void Test10()
        {
            mock.deleted = default;
            FamilliarsRecognition.Fact expected = new FamilliarsRecognition.Fact(factBuilder.WithId(1).WithFact("test").Build());

            manager.DeleteFact(expected);

            Assert.AreEqual(mock.deleted, expected);
        }

        public void TestAll()
        {
            Console.WriteLine("Running FamilliarsManager unit tests...");

            Test1();
            Test2();
            Test3();
            Test4();
            Test5();
            Test6();
            Test7();
            Test8();
            Test9();
            Test10();

            Console.WriteLine("");
        }
    }
}
