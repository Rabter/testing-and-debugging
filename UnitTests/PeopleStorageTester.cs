﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace UnitTests
{
    [TestClass]
    public class PeopleStorageTester
    {
        DBAccess.PeopleStorage storage;
        TestAccessor accessor;
        PersonInfo info;
        FactBuilder factBuilder;
        LogBuilder logBuilder;
        PersonBuilder personBuilder;
        DBAccess.Person person;
        List<DBAccess.Fact> facts;
        List<DBAccess.LogLine> logs;
        AccessorMockResult mock;

        public PeopleStorageTester()
        {
        }

        [TestInitialize]
        public void Init()
        {
            storage = new DBAccess.PeopleStorage();
            accessor = new TestAccessor();
            info = new PersonInfo();
            factBuilder = new FactBuilder();
            logBuilder = new LogBuilder();
            personBuilder = new PersonBuilder();

            mock = new AccessorMockResult();
            facts = new List<DBAccess.Fact>();
            facts.Add(factBuilder.WithId(1).WithFact("test fact1").Build());
            facts.Add(factBuilder.WithFact("test fact1").Build());
            logs = new List<DBAccess.LogLine>();
            logs.Add(logBuilder.WithId(1).WithDateTime(new DateTime(2000, 1, 1)).WithPersonPhrase("person test log1").WithBotPhrase("bot test log1").Build());
            logs.Add(logBuilder.WithDateTime(new DateTime(2000, 1, 2)).WithPersonPhrase("person test log2").WithBotPhrase("bot test log2").Build());

            person = personBuilder.WithId(1).WithName("Test Person").WithSex("m").WithBirthdate(new DateTime(2000, 1, 1)).Build();
            info.facts = facts;
            info.logs = logs;
            info.photo = default;

            accessor.info = info;
            accessor.person = personBuilder;
            storage.accessor = accessor;
        }

        [TestMethod]
        public void Test1()
        {
            int minId = 1, maxId = 5;
            accessor.minId = minId;
            accessor.maxId = maxId;

            int minres = storage.MinId();
            int maxres = storage.MaxId();

            Assert.AreEqual(minres, minId);
            Assert.AreEqual(maxres, maxId);
        }

        [TestMethod]
        public void Test2()
        {
            accessor.returnPerson = true;
            storage.LoadPeople(new List<int>(0));

            DBAccess.Person result;
            storage.GetPerson(1, out result);

            Assert.AreEqual(result, person);
        }

        [TestMethod]
        public void Test3()
        {
            accessor.returnPerson = true;
            storage.LoadPeople(new List<int>() { 1, 3, 5 });

            DBAccess.Person result;
            storage.GetPerson(1, out result);

            Assert.AreEqual(result, person);
        }

        [TestMethod]
        public void Test4()
        {
            accessor.returnPerson = true;
            storage.LoadPeople(new List<int>() { 1, 3, 5 });
            storage.LoadPeople(new List<int>() { 1, 3 });

            DBAccess.Person result;
            storage.GetPerson(1, out result);

            Assert.AreEqual(result, person);
        }

        [TestMethod]
        public void Test5()
        {
            accessor.returnPerson = true;
            storage.LoadPeople(new List<int>() { 1, 3, 5 });
            storage.LoadPeople(new List<int>() { 5, 3 });

            DBAccess.Person result;
            storage.GetPerson(1, out result);

            Assert.AreEqual(result, person);
        }

        [TestMethod]
        public void Test6()
        {
            accessor.returnPerson = true;
            storage.LoadPeople(new List<int>(0));

            List<DBAccess.Fact> result = storage.GetFacts(1);

            Assert.AreEqual(result, facts);
        }

        [TestMethod]
        public void Test7()
        {
            accessor.returnPerson = true;
            storage.LoadPeople(new List<int>(0));

            List<DBAccess.LogLine> result = storage.GetLogs(1);

            Assert.AreEqual(result, logs);
        }

        [TestMethod]
        public void Test8()
        {
            accessor.mock = mock;
            mock.pair = new Tuple<int, int>(0, 0);

            storage.AddPair(1, 2);

            Assert.AreEqual(mock.pair.Item1, 1);
            Assert.AreEqual(mock.pair.Item2, 2);
        }

        [TestMethod]
        public void Test9()
        {
            accessor.mock = mock;
            mock.log = logBuilder.WithId(1)
                .WithDateTime(new DateTime(1212, 12, 12))
                .WithBotPhrase("")
                .WithPersonPhrase( "").Build();
            DBAccess.LogLine sentLog = logBuilder.WithId(2)
                .WithDateTime(new DateTime(1010, 10, 10))
                .WithBotPhrase("bot test")
                .WithPersonPhrase("person test").Build();

            storage.AddLog(sentLog);

            Assert.AreEqual(mock.log, sentLog);
        }


        [TestMethod]
        public void Test10()
        {
            accessor.mock = mock;
            mock.fact = factBuilder.WithId(1).WithFact("").Build();
            DBAccess.Fact sentFact = factBuilder.WithId(2).WithFact("test fact").Build();

            storage.AddFact(sentFact);

            Assert.AreEqual(mock.fact, sentFact);
        }

        [TestMethod]
        public void Test11()
        {
            accessor.mock = mock;
            mock.fact = factBuilder.WithId(1).WithFact("").Build();
            DBAccess.Fact sentFact = factBuilder.WithId(2).WithFact("test fact").Build();

            storage.DeleteFact(sentFact);

            Assert.AreEqual(mock.deleted, sentFact);
        }

        public void TestAll()
        {
            Console.WriteLine("Running PeopleStorage unit tests...");

            Test1();
            Test2();
            Test3();
            Test4();
            Test5();
            Test6();
            Test7();
            Test8();
            Test9();
            Test10();
            Test11();

            Console.WriteLine("");
        }
    }
}