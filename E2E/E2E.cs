﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.Data.SqlClient;

namespace E2E
{
    [TestClass]
    public class E2E
    {
        public E2E()
        {
        }

        [TestMethod]
        public void Test1()
        {
            FamilliarsRecognition.FamilliarsRecognitionForm sut = new FamilliarsRecognition.FamilliarsRecognitionForm();
            sut.presenter.Open(@"D:\University\Testing\images");
            sut.presenter.AddFact(new FamilliarsRecognition.Fact(2, "test"));
            sut.presenter.NextImage();

            FactBuilder builder = new FactBuilder().WithId(2);
            List<int> expectedIds = new List<int>() { 2 };
            List<FamilliarsRecognition.Fact> expectedFacts = new List<FamilliarsRecognition.Fact>();
            expectedFacts.Add(new FamilliarsRecognition.Fact(builder.WithFact("Nerd").Build()));
            expectedFacts.Add(new FamilliarsRecognition.Fact(builder.WithFact("PhD at Physics").Build()));
            expectedFacts.Add(new FamilliarsRecognition.Fact(builder.WithFact("In Love with Amy").Build()));
            expectedFacts.Add(new FamilliarsRecognition.Fact(builder.WithFact("test").Build()));
            Assert.IsTrue(sut.presenter.loaded_ids.SequenceEqual(expectedIds));
            sut.presenter.familliarsManager.GetPerson(sut.presenter.loaded_ids[0], out FamilliarsRecognition.Person result);
            Assert.IsTrue(new HashSet<FamilliarsRecognition.Fact>(result.facts).SetEquals(expectedFacts));
        }

        public void TestAll()
        {
            Console.WriteLine("Running E2E tests...");

            Test1();

            Console.WriteLine("");
        }

        [TestInitialize]
        protected void reloadDb()
        {
            FileInfo fileInfo = new FileInfo("../../../Database/db_creator.sql");
            string script = fileInfo.OpenText().ReadToEnd();
            using (SqlConnection connection = new SqlConnection(Config.ConnectionString()))
            {
                Server server = new Server(new ServerConnection(connection));
                server.ConnectionContext.ExecuteNonQuery(script);
            }
        }
    }

}