﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace DBAccess
{
    public interface IAccessor
    {
        bool GetPerson(ref Person person);
        bool GetName(int id, out string name);
        bool GetImage(int id, out Image image);
        bool GetSex(int id, out string sex);
        bool GetBirthdate(int id, out DateTime birthdate);
        List<Fact> GetFacts(int id);
        List<LogLine> GetLogs(int person_id);
        bool AddPair(int id1, int id2);
        bool AddFact(Fact fact);
        bool AddLog(LogLine log);
        void DeleteFact(Fact fact);
        int MinId();
        int MaxId();
    }
}
