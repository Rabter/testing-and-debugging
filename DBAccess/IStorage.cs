﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace DBAccess
{
    public interface IStorage: IDisposable
    {
        int MinId();
        int MaxId();
        void LoadPeople(List<int> ids);
        bool GetPerson(int id, out Person person);
        List<Fact> GetFacts(int id);
        List<LogLine> GetLogs(int id);
        bool GetImage(int id, out Image image);
        bool AddPair(int id1, int id2);
        void AddFact(Fact fact);
        void DeleteFact(Fact fact);
        void AddLog(LogLine log);
    }
}
